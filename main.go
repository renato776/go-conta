package main

import (
    "fmt"
    "gopkg.in/yaml.v3"
    "github.com/360EntSecGroup-Skylar/excelize/v2"

    "database/sql"
    _ "github.com/godror/godror"

    "sort"
    "os"
    "strconv"
    "io/ioutil"
    "log"
)

type Entry map[string]float64
type Entrada map[string]string

type cv struct{
  v float64
  id int
}
type mini_cuenta struct{
  key string
  folio int
  Id float64
  value float64
}

type cuenta struct{
  folio int
  debe []cv
  haber []cv
  Id float64
  nombre string
  key string
}

type partida struct{
  Fecha string `yaml: fecha`
  Debe Entry `yaml: debe`
  Haber Entry `yaml: haber`
  Desc string `yaml: string`
  Tipo string `yaml: tipo`
  Monto float64 `yaml: monto`
  Metodo Entrada `yaml: metodo`
  Personal Entry `yaml: personal`
  Bonos Entry `yaml: bonos`
}

type diario struct {
  Owner string `yaml: owner`
  Mes string `yaml: mes`
  Company string `yaml: company`
  Diario []partida `yaml: diario`
}

var cuentas map[string]cuenta
var estilos []int

func sum(a float64, b float64) float64 { return a + b }
func divide(a float64, b float64) float64 { return a / b }
func multiply(a float64, b float64) float64 { return a * b }

func (c *diario) getDiario() *diario {
    yamlFile, err := ioutil.ReadFile("ejemplo-8.yaml")
    if err != nil {
        log.Printf("yamlFile.Get err   #%v ", err)
    }
    err = yaml.Unmarshal(yamlFile, c)
    if err != nil {
        log.Fatalf("Unmarshal: %v", err)
    }
    //c currently is of a normal diary type. We must process it and return a new refined version.
    for i:=0; i<len(c.Diario); i++{
        ptda := c.Diario[i];
        if ptda.Tipo == "" || ptda.Tipo == "otros" {continue;}
        c.Diario[i].Debe =  make(map[string]float64)
        c.Diario[i].Haber =  make(map[string]float64)
        switch ptda.Tipo {
          case "compra": {
            for k,v := range ptda.Metodo {
              switch v {
                case "total": c.Diario[i].Haber[k] = ptda.Monto;
                case "iva" : c.Diario[i].Haber[k] = divide(multiply(divide(ptda.Monto,1.12),12),100);
                default: c.Diario[i].Haber[k] = divide(ptda.Monto,1.12)
              }
            }
            c.Diario[i].Debe["compras"] = divide(ptda.Monto,1.12)
            c.Diario[i].Debe["iva por cobrar"] = divide(multiply(divide(ptda.Monto,1.12),12),100);}
          case "venta": {
            for k,v := range ptda.Metodo {
              switch v {
                case "total": c.Diario[i].Debe[k] = ptda.Monto;
                case "iva" : c.Diario[i].Debe[k] = divide(multiply(divide(ptda.Monto,1.12),12),100);
                default: c.Diario[i].Debe[k] = divide(ptda.Monto,1.12)
              }
            }
            c.Diario[i].Haber["ventas"] = divide(ptda.Monto,1.12)
            c.Diario[i].Haber["iva por pagar"] = divide(multiply(divide(ptda.Monto,1.12),12),100); }
          case "sueldos": {
            var igss float64
            var total float64
            patronal := make(map[string]float64)
            var suma_patronal float64
            suma_patronal = 0
            igss = 0
            for k,v := range ptda.Personal {
              c.Diario[i].Debe["sueldos "+k] = v
              igss = sum(igss, v)
              patronal[k] = multiply(v,0.1267)
            }
            total = igss
            for k,v := range ptda.Bonos {
              c.Diario[i].Debe["bonificacion "+k] = v
              total = sum(total, v)
            }
            cuota := multiply(igss,0.0483)
            c.Diario[i].Haber["retencion cuota laboral IGSS"] = cuota
            for k,_ := range ptda.Metodo {
              c.Diario[i].Haber[k] = sum(total, -1*cuota)
            }
            c.Diario[i].Desc = "Se cancelaron sueldos y bonificaciones del mes. Se retuvo cota patronal IGSS."
            var nptda partida
            nptda.Desc = "Pendiente de pagar, Cuotas Patronales del mes"
            nptda.Debe =  make(map[string]float64)
            nptda.Haber =  make(map[string]float64)
            for k,v := range patronal{
              nptda.Debe[k] = v
              suma_patronal = sum(suma_patronal, v)
            }
            nptda.Haber["cuentas por pagar"] = suma_patronal
            c.Diario = append(c.Diario, nptda) }
          default: continue;
        }
    }
    return c
}


func p_header(libro string, y int, value interface{}, f *excelize.File, width int) {
    f.SetCellValue(libro, coordinate(1,y), value)
    f.MergeCell(libro, coordinate(1,y), coordinate(width,y))
    f.SetCellStyle(libro, coordinate(1,y), coordinate(width,y), estilos[0])
}

func new_style(style string, f *excelize.File) int {
  i, err := f.NewStyle(style)
  if err != nil {
    return 0
  } else { return i }
}
func load_styles(f *excelize.File){
  estilos = []int{
    new_style(`{"font":{"bold":false},"alignment":{"horizontal":"center"}}`,f), //Header style
    new_style(`{"alignment":{"wrap_text":true, "horizontal":"justify"}, 
    "border":[{"type":"top", "style":1,"color":"000000"},
    {"type":"bottom", "style":1,"color":"000000"},
    {"type":"left", "style":1,"color":"000000"},
    {"type":"right", "style":1,"color":"000000"}]}`,f), //Description style
    new_style(`{"alignment":{"horizontal":"center"},"font":{"bold":true}, "border":
    [{"type":"top", "style":1,"color":"000000"},{"type":"bottom", "style":1,"color":"000000"},
    {"type":"left", "style":1,"color":"000000"},
    {"type":"right", "style":1,"color":"000000"}]}`,f), //Col header style
    new_style(`{"number_format":4,"border":[{"type":"left", "style":1,"color":"000000"},
    {"type":"right", "style":1,"color":"000000"}]}`,f),//Value cell. - No. 3
    new_style(`{"bold":false,"alignment":{"horizontal":"center"},"border":[
    {"type":"left", "style":1,"color":"000000"},{"type":"top", "style":1,"color":"000000"},
    {"type":"right", "style":1,"color":"000000"},{"type":"bottom", "style":1,"color":"000000"}]}`,f), //Partida
    new_style(`{"border":[{"type":"left", "style":1,"color":"000000"},{"type":"top", "style":1,"color":"000000"},
    {"type":"bottom", "style":6,"color":"000000"},{"type":"right", "style":1,"color":"000000"}],
    "number_format": 4}`,f),//Sumas Iguales
    new_style(`{"border":[{"type":"top", "style":1,"color":"000000"}]}`,f),//Nada, solo bottom border - No. 6
    new_style(`{"number_format":1,"border":[{"type":"left", "style":1,"color":"000000"},
    {"type":"right", "style":1,"color":"000000"}],"alignment":{"horizontal":"center"}}`,f), //folio - No.7
    new_style(`{"alignment":{"wrap_text":true, "horizontal":"center"}, 
    "border":[{"type":"bottom", "style":2,"color":"000000"}]}`,f), //T title - No. 8
    new_style(`{"number_format": 4,"border":[{"type":"right", "style":2,"color":"000000"}]}`,f),//T value - No. 9
    new_style(`{"number_format": 4,"border":[{"type":"left", "style":2,"color":"000000"}]}`,f),//T value - No. 10
    new_style(`{"number_format": 4,"border":[{"type":"right", "style":2,"color":"000000"},
    {"type":"top", "style":2,"color":"000000"},{"type":"bottom", "style":2,"color":"000000"}]}`,f),//T suma - No. 11
    new_style(`{"number_format": 4,"border":[{"type":"left", "style":2,"color":"000000"},
    {"type":"top", "style":2,"color":"000000"},{"type":"bottom", "style":2,"color":"000000"}]}`,f), //No. 12
    new_style(`{"alignment":{"horizontal":"center","vertical":"center"},"font":{"bold":true}, "border":
    [{"type":"top", "style":1,"color":"000000"},{"type":"bottom", "style":1,"color":"000000"},
    {"type":"left", "style":1,"color":"000000"},
    {"type":"right", "style":1,"color":"000000"}]}`,f), //B title - No. 13
    new_style(`{"number_format": 4,"border":[{"type":"right", "style":2,"color":"000000"},
    {"type":"top", "style":2, "color":"000000"},{"type":"left", "style":2,"color":"000000"},
    {"type":"bottom", "style":2,"color":"000000"}]}`,f)}//B sum - No. 14
}


func coordinate(a int, b int) string {
	return string(65 + a - 1)+strconv.Itoa(b)
}

func print_partida(p partida, format int, f *excelize.File, ctx int, id int) int{
  libro := "Diario"
  if id == 1 {
    for i:= 1; i<6; i++ {
      f.SetCellStyle(libro, coordinate(i,ctx), coordinate(i,ctx), estilos[2])
    }
    f.SetCellStyle(libro, coordinate(1,ctx), coordinate(1,ctx), estilos[4])
    f.SetCellValue(libro, coordinate(2,ctx), "M")
    f.SetCellValue(libro, coordinate(4,ctx), "DEBE")
    f.SetCellValue(libro, coordinate(5,ctx), "HABER")
  }else{
    for i:= 1; i<6; i++ {
      f.SetCellStyle(libro, coordinate(i,ctx), coordinate(i,ctx), estilos[4])
    }
  }
  f.SetCellValue(libro, coordinate(1,ctx), "Ptda#"+strconv.Itoa(id))
  f.SetCellValue(libro, coordinate(3,ctx), p.Fecha)
  ctx += 1
  var sumas float64
  sumas = 0
  fine := false
  /* Ordenando el Debe en base a ID*/
  var ctas []mini_cuenta
  ctas = []mini_cuenta{}
  for key, value := range p.Debe{
    c:= cuentas[key]
    ctas = append(ctas, mini_cuenta{key: c.nombre, value: value, folio: c.folio, Id: c.Id})
  }
  sort.Slice(ctas, func(i,j int) (bool){
    return ctas[i].Id < ctas[j].Id
  });
  for i:=0; i<len(ctas); i++{
    c := ctas[i]
    for i:= 1; i<6; i++ {
      f.SetCellStyle(libro, coordinate(i,ctx), coordinate(i,ctx), estilos[3])
    }
    f.SetCellStyle(libro, coordinate(2,ctx), coordinate(2,ctx), estilos[7]) //Use a different number format
    f.SetCellValue(libro, coordinate(2,ctx), c.folio)
    f.SetCellValue(libro, coordinate(3,ctx), c.key)
    f.SetCellValue(libro, coordinate(4,ctx), c.value)
    ctx += 1
    sumas = sum(sumas, c.value)
  }
  fine = sumas == 0

  ctas = []mini_cuenta{}
  for key, value := range p.Haber{
    c:= cuentas[key]
    ctas = append(ctas, mini_cuenta{key: c.nombre, value: value, folio: c.folio, Id: c.Id})
  }
  sort.Slice(ctas, func(i,j int) (bool){
    return ctas[i].Id < ctas[j].Id
  });
  for i:=0; i<len(ctas); i++{
    c := ctas[i]
    for i:= 1; i<6; i++ {
      f.SetCellStyle(libro, coordinate(i,ctx), coordinate(i,ctx), estilos[3])
    }
    f.SetCellStyle(libro, coordinate(2,ctx), coordinate(2,ctx), estilos[7]) //Use a different number format
    f.SetCellValue(libro, coordinate(2,ctx), c.folio)
    f.SetCellValue(libro, coordinate(3,ctx), c.key)
    f.SetCellValue(libro, coordinate(5,ctx), c.value)
    ctx += 1
    if fine {
      sumas = sum(sumas, c.value)
    }
  }
  f.SetCellValue(libro, coordinate(3,ctx), p.Desc)
  f.SetCellStyle(libro, coordinate(3,ctx), coordinate(3,ctx), estilos[1])
  if len(p.Desc) > 40 {
    f.SetRowHeight(libro,ctx,30) //Si la descripcion es mas grande que 40, se usa doble linea.
  }
  f.SetCellStyle(libro, coordinate(2,ctx), coordinate(2,ctx), estilos[3])
  f.SetCellValue(libro, coordinate(4,ctx), sumas)
  f.SetCellValue(libro, coordinate(5,ctx), sumas)
  for i:= 4; i<6; i++ {
    f.SetCellStyle(libro, coordinate(i,ctx), coordinate(i,ctx), estilos[5])
  }
  ctx += 1
  return ctx
}

func buscar_cuenta(nombre string, db *sql.DB) (float64, string){
  q:="select c1,c2,c3,nombre from cuenta where alias = '"+nombre+"'"
	rows, err := db.Query(q)
  if err != nil {
      panic(fmt.Errorf("error reading db: %w", err))
  }
  defer rows.Close()
  var name string
  name = ""
  for rows.Next(){
    var c1,c2,c3 int
    err = rows.Scan(&c1,&c2,&c3,&name)
    return ((float64) (c1*10000 +c2*100 +c3)), name
    if err != nil {
      panic(fmt.Errorf("error scanning db: %w", err))
    }
	}
  // No se encontro la cuenta. Se debera asignar un ID de ultimo.
  fmt.Println("Cuenta not found: ",nombre) //Casi todas no se encontraron :'(
  return (float64)(8000*10000 + len(cuentas) + 1), nombre
}
func add_folio(cta cuenta, folio int) (cuenta){
  return cuenta{Id: cta.Id, nombre: cta.nombre, folio: folio, debe: cta.debe, haber: cta.haber}
}
func order_cuentas() ([]cuenta){
  var jk []cuenta
  jk = []cuenta{}
  for _,value := range cuentas{
    jk = append(jk,value)
  }
  sort.Slice(jk, func(i, j int) bool {
    return jk[i].Id < jk[j].Id
  })
  return jk
}
func compile_diario(d diario, f *excelize.File, db *sql.DB) {
  for i := 1; i<=len(d.Diario); i++ {
    p := d.Diario[i-1]
    for key, value := range p.Debe{
      id,nombre := buscar_cuenta(key, db)
      if c, ok := cuentas[key]; ok{
        cuentas[key] = cuenta{key: key, Id: id, nombre: nombre, folio: c.folio, debe: append(c.debe, cv{value,i}), haber: c.haber}
      } else{
        cuentas[key] = cuenta{key: key, Id: id, nombre: nombre, folio: len(cuentas)+1, debe: []cv{cv{value, i}}, haber: []cv{}};
      }
    }
    for key, value := range p.Haber{
      id,nombre := buscar_cuenta(key, db)
      if c, ok := cuentas[key]; ok{
        cuentas[key] = cuenta{key: key, Id: id, nombre: nombre, folio: c.folio, debe: c.debe, haber: append(c.haber, cv{value,i})}
      } else{
        cuentas[key] = cuenta{key: key, Id: id, nombre: nombre, folio: len(cuentas)+1, haber: []cv{cv{value, i}}, debe: []cv{}};
      }
    }
  }
  //Los folios fueron otorgados al azar. Deberan ser corregidos:
  order := order_cuentas()
  for i:=1; i<=len(cuentas); i++ {
    cta := order[i-1]
    cuentas[cta.key] = add_folio(cta,i)
  }

  // Listo. Comenzamos la compilación para el Diario
  libro := "Diario"

  ctx := 5
  f.SetColWidth(libro, "A", "A", 8)
  f.SetColWidth(libro, "B", "B", 3)
  f.SetColWidth(libro, "C", "C", 40)
  f.SetColWidth(libro, "D", "D", 12)
  f.SetColWidth(libro, "E", "E", 12)
  for i := 1; i<=len(d.Diario); i++ {
    p := d.Diario[i-1]
    ctx = print_partida(p, 0, f, ctx, i)
  }
  f.SetCellStyle(libro, coordinate(1,ctx), coordinate(2,ctx), estilos[6])
}

func compile_T(nombre string, cta cuenta, ctx int, left bool, f *excelize.File) (int, bool){
  libro := "Mayor"

  var x int
  if left {
    x = 1
  } else {
    x = 6
  }

  f.SetCellValue(libro, coordinate(x + 2,ctx), "Folio No. "+strconv.Itoa(cta.folio))
  ctx += 1
  f.SetCellValue(libro, coordinate(x,ctx), "D")
  f.SetCellValue(libro, coordinate(x+3,ctx), "H")
  f.SetCellValue(libro, coordinate(x+1,ctx), nombre)
  f.MergeCell(libro, coordinate(x+1,ctx), coordinate(x+2,ctx))
  f.SetCellStyle(libro, coordinate(x+1,ctx), coordinate(x+2,ctx), estilos[8])
  if len(nombre) > 22 {
    f.SetRowHeight(libro,ctx,30) //Si el nombre de la cuenta es muy grande, se usa doble linea.
  }
  ctx +=1
  var suma_debe float64
  var suma_haber float64
  var max_ctx int
  suma_debe = 0
  suma_haber = 0
  baseline := ctx
  for i := 0; i<len(cta.debe); i++ {
    f.SetCellValue(libro, coordinate(x,ctx), "Ptda. "+strconv.Itoa(cta.debe[i].id))
    f.SetCellValue(libro, coordinate(x+1,ctx), cta.debe[i].v)
    f.SetCellStyle(libro, coordinate(x+1,ctx), coordinate(x+1,ctx), estilos[9])
    suma_debe = sum(suma_debe, cta.debe[i].v)
    ctx += 1
  }
  if len(cta.haber) > 0 {
    max_ctx = ctx
    ctx = baseline
  }
  for i := 0; i<len(cta.haber); i++ {
    f.SetCellValue(libro, coordinate(x+3,ctx), "Ptda. "+strconv.Itoa(cta.haber[i].id))
    f.SetCellValue(libro, coordinate(x+2,ctx), cta.haber[i].v)
    f.SetCellStyle(libro, coordinate(x+2,ctx), coordinate(x+2,ctx), estilos[10])
    suma_haber = sum(suma_haber, cta.haber[i].v)
    ctx += 1
  }
  if max_ctx > ctx { ctx = max_ctx }
  f.SetCellValue(libro, coordinate(x,ctx), "Suma")
  f.SetCellValue(libro, coordinate(x+1,ctx), suma_debe)
  f.SetCellStyle(libro, coordinate(x+1,ctx), coordinate(x+1,ctx), estilos[11])
  f.SetCellValue(libro, coordinate(x+2,ctx), suma_haber)
  f.SetCellStyle(libro, coordinate(x+2,ctx), coordinate(x+2,ctx), estilos[12])
  ctx += 1

  f.SetCellValue(libro, coordinate(x,ctx), "Saldo")
  var saldo float64
  saldo = 0
  if suma_debe > suma_haber {
    saldo = sum(suma_debe, -1*suma_haber)
    f.SetCellValue(libro, coordinate(x+1,ctx), saldo)
    f.SetCellValue(libro, coordinate(x+2,ctx), "Deudor")
  } else if suma_haber > suma_debe {
    saldo = sum(suma_haber, -1*suma_debe)
    f.SetCellValue(libro, coordinate(x+2,ctx), saldo)
    f.SetCellValue(libro, coordinate(x+1,ctx), "Acreedor")
  } else {
    f.SetCellValue(libro, coordinate(x+2,ctx), 0)
    f.SetCellValue(libro, coordinate(x+1,ctx), 0)
  }
  f.SetCellStyle(libro, coordinate(x+1,ctx), coordinate(x+1,ctx), estilos[9])
  f.SetCellStyle(libro, coordinate(x+2,ctx), coordinate(x+2,ctx), estilos[10])
  ctx += 1
  return ctx, !left
}

func compile_mayor(f *excelize.File) {
  libro := "Mayor"
  var w float64
  w = 11
  f.SetColWidth(libro, "B", "B", w)
  f.SetColWidth(libro, "C", "C", w)
  f.SetColWidth(libro, "G", "G", w)
  f.SetColWidth(libro, "H", "H", w)
  ctx := 5
  pre := 5
  left := true
  order := order_cuentas()
  for i:=0; i<len(order); i++{
    nombre := order[i].nombre
    cta := order[i]
    ctx, left = compile_T(nombre, cta, ctx, left, f)
    if left {
      if pre > ctx {
        ctx = pre
      }
      ctx += 1
      pre = ctx
    } else {
      aux := ctx
      ctx = pre
      pre = aux
    }
  }
}

func compile_balance(f *excelize.File) {
  libro := "Balance"
  ctx := 5
  f.SetColWidth(libro, "A", "A", 35)
  f.SetColWidth(libro, "B", "E", 12)
  f.SetCellValue(libro, coordinate(1,ctx), "CUENTAS")
  f.SetCellValue(libro, coordinate(2,ctx), "SUMAS")
  f.SetCellValue(libro, coordinate(4,ctx), "SALDOS")
  f.SetCellValue(libro, coordinate(2,ctx+1), "DEBE")
  f.SetCellValue(libro, coordinate(3,ctx+1), "HABER")
  f.SetCellValue(libro, coordinate(4,ctx+1), "DEBE")
  f.SetCellValue(libro, coordinate(5,ctx+1), "HABER")

  f.MergeCell(libro, coordinate(2,ctx), coordinate(3,ctx))
  f.MergeCell(libro, coordinate(4,ctx), coordinate(5,ctx))
  f.MergeCell(libro, coordinate(1,ctx), coordinate(1,ctx+1))

  f.SetCellStyle(libro, coordinate(1,ctx), coordinate(5,ctx+2), estilos[13])

  ctx += 2

  var suma_parcial_debe float64
  var suma_parcial_haber float64
  var suma_total_debe float64
  var suma_total_haber float64
  suma_parcial_debe = 0
  suma_parcial_haber = 0
  suma_total_debe = 0
  suma_total_haber = 0
  order := order_cuentas()
  for i:=0; i<len(order); i++{
    nombre := order[i].nombre
    cta := order[i]
    var suma_debe float64
    var suma_haber float64
    suma_debe = 0
    suma_haber = 0
    for i := 0; i<len(cta.debe); i++ {
      suma_debe = sum(suma_debe, cta.debe[i].v)
    }
    for i := 0; i<len(cta.haber); i++ {
      suma_haber = sum(suma_haber, cta.haber[i].v)
    }
    f.SetCellValue(libro, coordinate(1,ctx), nombre)
    if suma_debe > 0 {f.SetCellValue(libro, coordinate(2,ctx), suma_debe)}
    if suma_haber > 0 {f.SetCellValue(libro, coordinate(3,ctx), suma_haber)}
    suma_parcial_debe = sum(suma_parcial_debe, suma_debe)
    suma_parcial_haber = sum(suma_parcial_haber, suma_haber)
    if suma_debe >= suma_haber {
      saldo := sum(suma_debe, -1*suma_haber)
      f.SetCellValue(libro, coordinate(4,ctx), saldo)
      suma_total_debe = sum(suma_total_debe, saldo)
    } else {
      saldo := sum(suma_haber, -1*suma_debe)
      f.SetCellValue(libro, coordinate(5,ctx), saldo)
      suma_total_haber = sum(suma_total_haber, saldo)
    }

    f.SetCellStyle(libro, coordinate(1,ctx), coordinate(5,ctx), estilos[3])
    ctx += 1
  }
  f.SetCellValue(libro, coordinate(1,ctx), "SUMAS IGUALES")
  f.SetCellStyle(libro, coordinate(1,ctx), coordinate(1,ctx), estilos[13])
  f.SetCellValue(libro, coordinate(2,ctx), suma_parcial_debe)
  f.SetCellValue(libro, coordinate(3,ctx), suma_parcial_haber)
  f.SetCellValue(libro, coordinate(4,ctx), suma_total_haber)
  f.SetCellValue(libro, coordinate(5,ctx), suma_total_debe)
  for i:=2; i<6; i++{
    f.SetCellStyle(libro, coordinate(i,ctx), coordinate(i,ctx), estilos[14])
  }
}

func print_header(libro string, f *excelize.File, d diario, width int){
  p_header(libro, 1, "Libro "+libro+" de la empresa "+d.Company, f, width)
  p_header(libro, 2, "Propiedad de "+d.Owner, f, width)
  p_header(libro, 3, "Correspondiente al "+d.Mes, f, width)
}
func main() {
    databaseDSN, ok := os.LookupEnv("DATABASE_DSN")
    if !ok {
      panic(fmt.Errorf("Connection string not found."))
    }

    db, err := sql.Open("godror", databaseDSN)
    if err != nil {
      panic(fmt.Errorf("error opening db: %w", err))
    }

    err = db.Ping()
    if err != nil {
      panic(fmt.Errorf("error pinging db: %w", err))
    }

    cuentas = make(map[string]cuenta)
    var d diario
    d.getDiario()

    f := excelize.NewFile()
    load_styles(f)
    f.SetSheetName("Sheet1","Diario")
    f.NewSheet("Mayor")
    f.NewSheet("Balance")
    print_header("Diario",f,d,5)
    print_header("Mayor",f,d,8)
    print_header("Balance",f,d,5)
    compile_diario(d,f,db)
    compile_mayor(f)
    compile_balance(f)
    if err := f.SaveAs("Ejemplo-8.xlsx"); err != nil { // Guardamos el archivo.
        fmt.Println(err)
    }
}

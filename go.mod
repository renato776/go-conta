module gitlab.com/renato776/go-conta

go 1.15

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1 // indirect
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.3.2
	github.com/godror/godror v0.24.3
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
